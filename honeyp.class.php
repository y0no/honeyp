<?php

class HoneyP {

	private static $_instance = null;
	private $_isNotSafe = false;
	private $_attackType = "";
	private $_logName = "";


	public function __construct($logName){
		$this->_logName = $logName;
	}

	public static function getInstance($logName){
		if(is_null(self::$_instance)){
			self::$_instance = new HoneyP($logName);
		}

		return self::$_instance;
	}

	public function checkRequest(){

		/*  We merge GET and POST request */
		$verifQueue = array_merge($_GET, $_POST, $_COOKIE);

		/* Do this to iterate through some array */
		$verifQueue = new RecursiveIteratorIterator(new RecursiveArrayIterator($verifQueue));


		/* For each element of the array, we check SQLi and XSS */
		foreach($verifQueue as $verifKey => $verifElement){
			$verifElement = urldecode($verifElement);
			if(!$this->checkAll($verifElement)){
				$this->_isNotSafe = true;
			}
		}

		/* If there is a problem, we do something... or not. */
		if($this->_isNotSafe){
			$this->logRequest();
			$this->doAction();
		}		
	}



	private function checkAll($request) {

		if(!$this->checkSQLi($request)){
			$this->_attackType = "SQLi";
			return false;

		} else if(!$this->checkXSS($request)){
			$this->_attackType = "XSS";
			return false;

		} else if (!$this->checkNullByte($request)){
			$this->_attackType = "NullByte";
			return false;
		}

		return true;
	}


	private function checkXSS($request) {
		$strings = ['javascript', '<', '>', 'text/html', ';base64'];

		foreach($strings as $string){
			if(strpos($request, $string) !== false)
				return false;
		}

		return true;
	}


	private function checkSQLi($request){
		$match = preg_match('/(\%|\'1|"1|1=1|1=0|ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})/', $request, $result);
		
		if($match != 0 && $match != false)
			return false;
		return true;
	}


	private function checkNullByte($request){
		if(strpos($request, "\0") !== false)
			return false;
		return true;
	}


	private function logRequest(){
		if($_SERVER['REQUEST_METHOD'] == 'GET'){
			$payload = $_SERVER['QUERY_STRING'];
		
		} else {
			$payload = json_encode($_POST);
		}


		$message  =  $this->getDate().' '.$this->_attackType.' '.$_SERVER['REMOTE_ADDR'].' '.$_SERVER['SCRIPT_NAME'].' '.$_SERVER['REQUEST_METHOD'].' '.$payload."\n";
		//$message .=  json_encode($_COOKIE)."\n";
		
		if (!error_log($message, 3, 'honeyp_'.$this->_logName.'.log'))
			die();
	}

	private function getDate() {
		return date("m/d/y_H:i");
	}

	private function doAction() {
	}
}